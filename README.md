![Johnson_challengue_image](http://bolsamexicanadevalores.com.mx/wp-content/uploads/jnj.jpg)

# Johnson_challengue

1. [Requirements](#Requirements)
2. [Integration](#Integration)
3. [Folder navigation](#Folder navigation)
4. [Decisions](#Decisions)

## Requirements
- iOS 8.0+ 

## Integration
- You have to run your podfile with "pod install" command, configuring your workspace.

## Folder navigation

* **Constants.swift:** Global constants
* **Device.swift:** Object class with devices information
* **Services.swift:** Api-Rest methods.
* **AppDelegate.swift**
* **Controllers**
      * **DevicesListViewController.swift:** List of devices
      * **DeviceDetailViewController.swift:** Device information
      * **AddDeviceViewController.swift:** Add new device

## Decisions

I chose Alamofire to connect to the server and SwiftyJSON for deserialize the json received because both are estables and simples, and I prefer to store data with Realm by the same decision.