//
//  Services.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 31/8/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift

private let baseURL = "http://private-1cc0f-devicecheckout.apiary-mock.com/"

struct EndPoint {
    static let getDevices = "devices"
}

class HelperServicesCall
{
    static let sharedInstance = HelperServicesCall()
    
    func addDevice(device: Device, completion:(response: AnyObject!) ->Void)
    {
        Services.sharedInstance.addDevice(device) { (response) in
            
            if response is NSError
            {
                let error = response as! NSError
                print(error.localizedDescription)
                
                let realm = try! Realm()
                
                try! realm.write {
                    realm.create(Device.self, value: device, update: true)
                }
            }
            else
            {
                let json = JSON(response)
                print(json)
                let realm = try! Realm()
                let device = Device()
                device.fill(json)
                device.synchronized = true
                
                try! realm.write {
                    realm.create(Device.self, value: device, update: true)
                }
            }
            completion(response: response)
        }
    }
    
    func updateDevice(device: Device, completion:(response: AnyObject!) ->Void)
    {
        let realm = try! Realm()
        
        Services.sharedInstance.updateDevice(device) { (response) in
            
            if response is NSError
            {
                let error = response as! NSError
                print(error.localizedDescription)
                
                if error.localizedDescription == "The Internet connection appears to be offline."
                {
                    try! realm.write {
                        device.action = SyncAction.updateDevice
                        device.synchronized = false
                        realm.create(Device.self, value: device, update: true)
                    }
                }
                else
                {
                    try! realm.write {
                        realm.create(Device.self, value: device, update: true)
                    }
                }
                completion(response: response)
            }
        }
    }
}

class Services
{
    static let sharedInstance = Services()
    
    func requestDevices(completion:(response: AnyObject!) ->Void)
    {
        apiRequest(.GET, endPoint: EndPoint.getDevices, parameters: [:], completion: completion)
    }
    
    func addDevice(device: Device, completion:(response: AnyObject!) ->Void)
    {
        let parameters = [
            "device": device.device,
            "manufacturer": device.manufacturer,
            "os": device.os
        ]
        
        apiRequest(.POST, endPoint: EndPoint.getDevices, parameters: parameters, completion: completion)
    }
    
    func updateDevice(device: Device, completion:(response: AnyObject!) ->Void)
    {
        var parameters = Dictionary<String, AnyObject>()
        
        if device.isCheckedOut
        {
            parameters = ["isCheckedOut": false]
        }
        else
        {
            parameters = [
                "lastCheckedOutBy": device.lastCheckedOutBy,
                "lastCheckedOutDate": device.lastCheckedOutDate,
                "isCheckedOut": true
            ]
        }
        
        apiRequest(.POST, endPoint: EndPoint.getDevices + "/" + device.id.description, parameters: parameters, completion: completion)
    }
    
    func deleteDevice(device: Device, completion:(response: AnyObject!) ->Void)
    {
        apiRequest(.DELETE, endPoint: EndPoint.getDevices + "/" + device.id.description, parameters: [:], completion: completion)
    }
    
    private func apiRequest(method: Alamofire.Method, endPoint: String, parameters: [String : AnyObject], completion:(response: AnyObject!) ->Void)
    {
        Alamofire.request(method, baseURL + endPoint, parameters: parameters).responseJSON{ response in
            switch response.result {
            case .Success:
                completion(response: response.result.value)
                
            case .Failure(let error):
                completion(response: error)
            }
        }
    }
}
