//
//  Device.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 30/08/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Device: Object {
    dynamic var device = ""
    dynamic var os = ""
    dynamic var manufacturer = ""
    dynamic var id = 0
    dynamic var lastCheckedOutBy = ""
    dynamic var lastCheckedOutDate = ""
    dynamic var isCheckedOut = false
    dynamic var synchronized = false
    dynamic var action = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func fill(json: JSON)
    {
        if let jsonDevice = json["device"].string{
            device = jsonDevice
        }
        
        if let jsonOs = json["os"].string{
            os = jsonOs
        }
        
        if let jsonDevice = json["device"].string{
            device = jsonDevice
        }
        
        if let jsonManufacturer = json["manufacturer"].string{
            manufacturer = jsonManufacturer
        }
        
        if let jsonId = json["id"].int{
            id = jsonId
        }
        
        if let jsonLastCheckedOutBy = json["lastCheckedOutBy"].string{
            lastCheckedOutBy = jsonLastCheckedOutBy
        }
        
        if let jsonLastCheckedOutDate = json["lastCheckedOutDate"].string{
            lastCheckedOutDate = jsonLastCheckedOutDate
        }
        
        if let jsonIsCheckedOut = json["isCheckedOut"].bool{
            isCheckedOut = jsonIsCheckedOut
        }
    }
}