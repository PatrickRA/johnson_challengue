//
//  AddDeviceViewController.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 30/8/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

class AddDeviceViewController: UIViewController
{
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak private var manufacturerField: UITextField!
    @IBOutlet weak private var osField: UITextField!
    @IBOutlet weak private var deviceField: UITextField!

    var devicesListDelegate: DevicesList?
    
    @IBAction func dismiss(sender: AnyObject) {
        if (manufacturerField.text!.isEmpty && osField.text!.isEmpty && deviceField.text!.isEmpty)
        {
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: nil, message: "You'll lose all entered data. Do you want to continue?", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler:{ (UIAlertAction) in
                
                self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
            }))
            
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction private func save(sender: AnyObject) {
        if (!manufacturerField.text!.isEmpty && !osField.text!.isEmpty && !deviceField.text!.isEmpty)
        {
            let realm = try! Realm()
            
            let device = Device()
            device.id = (realm.objects(Device).max("id") as Int?)! + 1
            device.device = deviceField.text!
            device.os = osField.text!
            device.manufacturer = manufacturerField.text!
            device.action = SyncAction.addDevice
            device.synchronized = false
            
            addDevice(device)
        }
        else
        {
            let alert = UIAlertController(title: nil, message: "You have to complete all fields", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func addDevice (device: Device)
    {
        if self.activityIndicator != nil{
            self.activityIndicator.startAnimating()
        }
        
        HelperServicesCall().addDevice(device) { (response) in
            self.clearFields()
            if self.activityIndicator != nil{
                self.activityIndicator.stopAnimating()
            }
            
            self.devicesListDelegate?.updateDevicesList(false)
        }
    }
    
    private func clearFields()
    {
        if deviceField != nil{
            deviceField.text = ""
        }
        
        if manufacturerField != nil{
            manufacturerField.text = ""
        }
        
        if osField != nil{
            osField.text = ""
        }
    }
}
