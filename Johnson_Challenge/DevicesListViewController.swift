//
//  DevicesListViewController.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 30/08/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift

protocol DevicesList
{
    func updateDevicesList(recall: Bool)
}

class DevicesListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DevicesList
{
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak private var devicesTableView: UITableView!
    
    private let cellIdentifier = "DeviceCell"
    private var devices: Array<Device> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        devicesTableView.tableFooterView = UIView(frame: CGRectZero)
        showStoredDevices(true)
        requestDevices()
    }
    
    func updateDevicesList(recall: Bool)
    {
        showStoredDevices(recall)
    }
    
    private func showStoredDevices(update: Bool)
    {
        let realm = try! Realm()
        self.devices = []
        
        let storedDevices = Array(realm.objects(Device.self))
        if storedDevices.count > 0{
            activityIndicator.stopAnimating()
        }
        
        for device in storedDevices{
            self.devices.append(device)
            if !device.synchronized{
                switch device.action {
                case SyncAction.addDevice:
                    if update{
                        HelperServicesCall.sharedInstance.addDevice(device) { (response) in}
                    }
                case SyncAction.updateDevice:
                    if update{
                        HelperServicesCall.sharedInstance.updateDevice(device) { (response) in}
                    }
                case SyncAction.deleteDevice:
                    self.devices.removeLast()
                    if update{
                        deleteDevice(device)
                    }
                default:
                    break
                }
            }
        }
        
        self.devicesTableView.reloadData()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.setFullWidthToSeparatorLine()
        cell.accessoryType = .DisclosureIndicator
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! DeviceCell
        let device = devices[indexPath.row]

        cell.deviceLabel.text = device.device
        cell.checkedLabel.text = device.lastCheckedOutBy
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            let device = devices[indexPath.row]
            let alertController = UIAlertController(title: nil, message: "Do you want delete device " + device.device + "?", preferredStyle: .Alert)
            let delete = UIAlertAction(title: "Yes", style: .Destructive, handler: { action in
                self.deleteDevice(device)
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            
                tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
            })
            
            alertController.addAction(delete)
            alertController.addAction(cancel)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "pushToDeviceDetail"
        {
            let destination = segue.destinationViewController as! DeviceDetailViewController
            destination.device = devices[devicesTableView.indexPathForSelectedRow!.row]
            destination.devicesListDelegate = self
        }
        else if segue.identifier == "pushToAddDevice"
        {
            let destinationNavigationController = segue.destinationViewController as! UINavigationController
            let destination: AddDeviceViewController = destinationNavigationController.topViewController as! AddDeviceViewController
            destination.devicesListDelegate = self
        }
    }
    
    //MARK: - API REST
    private func requestDevices()
    {
        Services.sharedInstance.requestDevices { (response) in

            if response is NSError
            {
                self.activityIndicator.stopAnimating()
                let error = response as! NSError
                print(error.localizedDescription)
            }
            else
            {
                let json = JSON(response)
                print(json)
                let realm = try! Realm()
                
                for (_,subJson):(String, JSON) in json {
                    let device = Device()
                    device.fill(subJson)
                    device.synchronized = true
                    
                    try! realm.write {
                        realm.create(Device.self, value: device, update: true)
                    }
                    self.devices = Array(realm.objects(Device.self))
                }
                
                self.activityIndicator.stopAnimating()
                self.devicesTableView.reloadData()
            }
        }
    }
    
    private func deleteDevice(device: Device)
    {
        if self.activityIndicator != nil{
            self.activityIndicator.startAnimating()
        }
        
        let realm = try! Realm()
        
        Services.sharedInstance.deleteDevice(device) { (response) in

            if response is NSError
            {
                let error = response as! NSError
                print(error.localizedDescription)
                
                try! realm.write {
                    device.action = SyncAction.deleteDevice
                    device.synchronized = false
                    realm.create(Device.self, value: device, update: true)
                }
                
                self.showStoredDevices(false)
                if self.activityIndicator != nil{
                    self.activityIndicator.stopAnimating()
                }

            }
            else
            {
                try! realm.write {
                    realm.delete(device)
                }
                self.showStoredDevices(true)
                if self.activityIndicator != nil{
                    self.activityIndicator.stopAnimating()
                }
            }
        }
    }
}

class DeviceCell: UITableViewCell{
    @IBOutlet weak private var deviceLabel: UILabel!
    @IBOutlet weak private var checkedLabel: UILabel!
}

extension UITableViewCell{
    func setFullWidthToSeparatorLine(){
        if self.respondsToSelector(Selector("setSeparatorInset:")){
            separatorInset = UIEdgeInsetsZero
        }
        
        if self.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")){
            preservesSuperviewLayoutMargins = false
        }
        
        if self.respondsToSelector(Selector("setLayoutMargins:")){
            layoutMargins = UIEdgeInsetsZero
        }
    }
}

