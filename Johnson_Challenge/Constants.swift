//
//  Constants.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 29/8/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import Foundation

struct SyncAction{
    static let addDevice = "addDevice"
    static let deleteDevice = "deleteDevice"
    static let updateDevice = "updateDevice"
}
