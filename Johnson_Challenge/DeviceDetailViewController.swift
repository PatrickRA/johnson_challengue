//
//  DeviceDetailViewController.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 30/08/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RealmSwift

class DeviceDetailViewController: UIViewController
{
    @IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak private var deviceLabel: UILabel!
    @IBOutlet weak private var osLabel: UILabel!
    @IBOutlet weak private var manufacturerLabel: UILabel!
    @IBOutlet weak private var lastCheckedOutLabel: UILabel!
    @IBOutlet weak private var buttonSpace: NSLayoutConstraint!
    @IBOutlet weak private var checkButton: UIButton!
    
    var devicesListDelegate: DevicesList?
    private var nameField: UITextField!
    
    var device = Device()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Detail"
        showDeviceDetail()
    }
    
    private func showDeviceDetail()
    {
        print(self.device)
        
        deviceLabel.text = "Device: " + self.device.device
        osLabel.text = "OS: " + self.device.os
        manufacturerLabel.text = "Manufacturer: " + self.device.manufacturer
        if !self.device.lastCheckedOutBy.isEmpty && !self.device.lastCheckedOutDate.isEmpty {
            lastCheckedOutLabel.text = "Last Checked Out: " + self.device.lastCheckedOutBy + " on " + self.device.lastCheckedOutDate
            buttonSpace.constant = 0
        }
        else{
            lastCheckedOutLabel.text = ""
            buttonSpace.constant = -20
        }

        checkButton.removeTarget(nil, action:nil, forControlEvents:UIControlEvents.AllEvents)

        if self.device.isCheckedOut{
            checkButton.setTitle("Check in", forState: .Normal)
            checkButton.addTarget(self, action: #selector(checkin), forControlEvents: .TouchUpInside)
        }
        else{
            checkButton.setTitle("Check out", forState: .Normal)
            checkButton.addTarget(self, action: #selector(checkout), forControlEvents: .TouchUpInside)
        }
    }
    
    func updateDevice(device: Device)
    {
        if self.activityIndicator != nil{
            self.activityIndicator.startAnimating()
        }
        
        HelperServicesCall().updateDevice(device) { (response) in
            if self.activityIndicator != nil{
                self.activityIndicator.stopAnimating()
            }
            
            self.showDeviceDetail()
            self.devicesListDelegate?.updateDevicesList(false)
        }
    }
    
    func checkin()
    {
        let realm = try! Realm()
        try! realm.write {
            self.device.isCheckedOut = false
            self.device.lastCheckedOutBy = ""
            self.device.lastCheckedOutDate = ""
        }
    
        self.updateDevice(self.device)
    }
    
    func checkout()
    {
        func configurationTextField(textField: UITextField!)
        {
            textField.placeholder = "Name"
            nameField = textField
        }
        
        let alert = UIAlertController(title: nil, message: "Enter your name:", preferredStyle: .Alert)
        
        alert.addTextFieldWithConfigurationHandler(configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Done", style: .Default, handler:{ (UIAlertAction) in
            
            let realm = try! Realm()
            try! realm.write {
                self.device.lastCheckedOutBy = self.nameField.text!
                
                let formatter = NSDateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                self.device.lastCheckedOutDate = formatter.stringFromDate(NSDate())
                
                self.device.isCheckedOut = true
            }
            
            self.updateDevice(self.device)
        }))
        
        alert.view.setNeedsLayout()
        self.presentViewController(alert, animated: true, completion: nil)
    }
}


