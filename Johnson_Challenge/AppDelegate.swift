//
//  AppDelegate.swift
//  Johnson_Challenge
//
//  Created by Patricio José Rodríguez Arias on 30/08/16.
//  Copyright © 2016 Patricio José Rodríguez Arias. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}

